package com.example.inspired.advisor.utils;

import com.example.inspired.advisor.models.ChartItemModel;
import com.example.inspired.advisor.models.ContentItemModel;
import com.example.inspired.advisor.models.MainDrugModel;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;

import java.util.ArrayList;
import java.util.Map;

/**
 * Created by inspired on 18.12.16.
 */

public class StaticDataUtils {
    public static MainDrugModel setStaticMainData(int number, String title){
        return new MainDrugModel(number, setChartItem(number, title), setStaticContent());
    }

    public static ArrayList<ChartItemModel> setStaticCharts() {
        ArrayList<ChartItemModel> data = new ArrayList<>();

        for(Map.Entry<Integer, MainDrugModel> entry : DataContext.getEntrySet()) {
            data.add(entry.getValue().getChartItem());
        }

        return data;
    }

    private static ArrayList<ContentItemModel> setStaticContent() {
        ArrayList<ContentItemModel> elements = new ArrayList<>();

        elements.add(new ContentItemModel("BE VERY CAREFUL COMING OFF WELLBUTRIN", "I just want to make sure that ppl know that you should be very careful about coming off this med. You can have severe withdrawal symptoms, as I did, just from a small dose. I was only taking 100mg a day for about 6 mos. I foolishly just stopped taking it. (Because I started Strattera and worried about mixing the two.) I started having suicidal and then homocidal feelings. I had flu-like symptoms, migraines, extreme fatigue, muscle aches and God only knows what else Ive been feeling thats attributed to this. I was off it for at least a whole week and a half before I figured out that what I was having were withdrawal symptoms. The scariest part was that I was watching tv, a detective program about a murder and \"suddenly I understood\" what made ppl want to kill and how and why they did it. I thought I was losing my mind for sure. Very scary. (Last week, I was on the computer one night looking at sites that told you how to kill yourself!)"));
        elements.add(new ContentItemModel("BE VERY CAREFUL ABOUT STOPPING THIS MEDICATION!", " Hey all, I am taking 16-20mg of Suboxone a day for dependance of Vicodin, Percocet. I started with this about 10 months ago and it is a miracle drug. However, the Suboxone made me constipation to the point where id go to the bathroom once a week! I was in pain and it seemed to dry out my intestines. I even had to use suppositories in some painful cases. Laugh it up, till it happens to you."));
        elements.add(new ContentItemModel("Hey all", "Anyways, I tried every remedy to get rid of the constipation. Senna tea, increased fiber, stool softeners (which kinda worked) and laxatives. This continued up until about 2 weeks ago, my doctor recommended Miralax. This is a powder that dissolves into any liquid and is tasteless. I didnt think it would work, but it did. I was so happy to have a soft bowel movement I almost screamed for joy and even told my parents. What a goof I am. Miralax is OTC and is the best laxative. I had constipation as a child also so this is coming from experience. So, MIRALAX is the cure to Suboxone Constipation. Please try this to help you all out, for we will get through this together. Good luck and take care of yourselves."));

        return elements;
    }

    private static ChartItemModel setChartItem(int number, String title){
        ArrayList<Integer[]> data = new ArrayList<>();
        switch (number){
            case 0:
                data.add(new Integer[]{0, 12});
                data.add(new Integer[]{1, 66});
                data.add(new Integer[]{2, 100});
                data.add(new Integer[]{3, 1050});
                data.add(new Integer[]{6, 401});
                break;
            case 1:
                data.add(new Integer[]{0, 401});
                data.add(new Integer[]{1, 190});
                data.add(new Integer[]{2, 175});
                data.add(new Integer[]{3, 201});
                data.add(new Integer[]{6, 120});
                break;
            default:
                data.add(new Integer[]{1, 58});
                data.add(new Integer[]{2, 70});
                data.add(new Integer[]{0, 45});
                data.add(new Integer[]{6, 301});
                data.add(new Integer[]{3, 1010});
                break;
        }

        return new ChartItemModel(number, title, data);
    }

    public static void setChart(LineChart chart, LineDataSet dataSet) {
        LineData lineData = new LineData(dataSet);
        dataSet.setColors(ColorTemplate.COLORFUL_COLORS);
        dataSet.setMode(LineDataSet.Mode.CUBIC_BEZIER);
        dataSet.setDrawCircles(true);
        dataSet.setDrawFilled(true);
        chart.setData(lineData);
        chart.animateXY(2000, 2000);
        chart.invalidate();
    }
}
