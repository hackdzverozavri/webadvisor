package com.example.inspired.advisor.fragments;

import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.inspired.advisor.adapters.ChartsAdapter;
import com.example.inspired.advisor.R;
import com.example.inspired.advisor.utils.DataContext;
import com.example.inspired.advisor.utils.StaticDataUtils;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by inspired on 17.12.16.
 */

public class ResultsChartsFragment extends BaseFragment {
    @BindView(R.id.recycler_charts)
    RecyclerView recyclerView;

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_results_charts, container, false);
        ButterKnife.bind(this, rootView);

        init();

        return rootView;
    }

    private void init() {
        // Set the static data.
        DataContext.add(0, StaticDataUtils.setStaticMainData(0, "Headache"));
        DataContext.add(1, StaticDataUtils.setStaticMainData(1, "Depression"));
        DataContext.add(2, StaticDataUtils.setStaticMainData(2, "Infection"));

        // init the recycler
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity())); // Adding a grid system in 2 columns.
        recyclerView.setAdapter(new ChartsAdapter(getContext()));
    }
}
