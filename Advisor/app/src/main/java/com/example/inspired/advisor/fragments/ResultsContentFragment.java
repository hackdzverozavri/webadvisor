package com.example.inspired.advisor.fragments;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.inspired.advisor.R;
import com.example.inspired.advisor.adapters.ContentAdapter;
import com.example.inspired.advisor.models.ChartItemModel;
import com.example.inspired.advisor.utils.DataContext;
import com.example.inspired.advisor.utils.StaticDataUtils;
import com.github.mikephil.charting.charts.LineChart;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by inspired on 17.12.16.
 */

public class ResultsContentFragment extends BaseFragment {
    @BindView(R.id.recycler_items_content)
    RecyclerView recyclerView;
    @BindView(R.id.category_name)
    TextView categoryName;
    @BindView(R.id.chart)
    LineChart chart;

    private int categoryId;
    private static final String ARG_CATEGORY_TYPE = "category type";

    public ResultsContentFragment() {
        // Required empty public constructor
    }

    public static ResultsContentFragment newInstance(int categoryId) {
        ResultsContentFragment fragment = new ResultsContentFragment();
        Bundle args = new Bundle();

        args.putInt(ARG_CATEGORY_TYPE, categoryId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            categoryId = getArguments().getInt(ARG_CATEGORY_TYPE);
        }
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_results_content, container, false);
        ButterKnife.bind(this, rootView);
        ChartItemModel chartItem = DataContext.get(categoryId).getChartItem();

        categoryName.setText(chartItem.getCategoryName());
        ViewGroup.LayoutParams params = chart.getLayoutParams();
        // Changes the height and width to the specified *pixels*
        params.height = 700;
        chart.setLayoutParams(params);
        StaticDataUtils.setChart(chart, chartItem.getData());

        init();

        return rootView;
    }

    private void init() {
        // init the recycler
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(new ContentAdapter(getContext(), categoryId));
    }
}
