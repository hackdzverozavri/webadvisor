package com.example.inspired.advisor.models;

import java.util.ArrayList;

/**
 * Created by inspired on 18.12.16.
 */

public class MainDrugModel {
    private int id;
    private ChartItemModel chartItem;
    private ArrayList<ContentItemModel> contentItems;

    public MainDrugModel(int id, ChartItemModel chartItem, ArrayList<ContentItemModel> contentItems){
        this.id = id;
        this.chartItem = chartItem;
        this.contentItems = contentItems;
    }

    public ChartItemModel getChartItem() {
        return chartItem;
    }

    public void setChartItem(ChartItemModel chartItem) {
        this.chartItem = chartItem;
    }

    public ArrayList<ContentItemModel> getContentItem() {
        return contentItems;
    }

    public void setContentItem(ArrayList<ContentItemModel> contentItems) {
        this.contentItems = contentItems;
    }
}
