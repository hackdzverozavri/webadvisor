package com.example.inspired.advisor.utils;

import com.example.inspired.advisor.models.MainDrugModel;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Created by inspired on 18.12.16.
 */

public class DataContext {
    private static HashMap<Integer, MainDrugModel> mainData = new HashMap<>();

    public static void add(int position, MainDrugModel drug){
        mainData.put(position, drug);
    }

    public static MainDrugModel get(Integer key){
        return mainData.get(key);
    }

    public static Set<Map.Entry<Integer, MainDrugModel>> getEntrySet(){
        return mainData.entrySet();
    }
}
