package com.example.inspired.advisor.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;

import com.example.inspired.advisor.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by inspired on 17.12.16.
 */

public class AdvisorFormFragment extends BaseFragment {
    @BindView(R.id.search_field)
    EditText searchField;
    @BindView(R.id.headache)
    CheckBox headache;
    @BindView(R.id.depression)
    CheckBox depression;
    @BindView(R.id.infection)
    CheckBox infection;
    @BindView(R.id.insomnia)
    CheckBox insomnia;
    @BindView(R.id.tiredness)
    CheckBox tiredness;

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_advisor_form, container, false);
        ButterKnife.bind(this, rootView);

        return rootView;
    }

    @OnClick(R.id.search_button)
    public void onClick(){
        /* System.out.println(searchField.getText());
        System.out.println(headache.isChecked());*/

        showFragmentAndAddToBackStack(new ResultsChartsFragment(),
                "Chart Results",
                R.id.container);
    }
}
