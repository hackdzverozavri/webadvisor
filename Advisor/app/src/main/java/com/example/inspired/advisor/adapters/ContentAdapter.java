package com.example.inspired.advisor.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.inspired.advisor.R;
import com.example.inspired.advisor.models.ContentItemModel;
import com.example.inspired.advisor.utils.DataContext;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by inspired on 18.10.16.
 */

public class ContentAdapter extends BaseRecyclerViewAdapter<ContentAdapter.ViewHolder, ContentItemModel> {
    private int categoryId;
    private final Context context;

    public ContentAdapter(Context context, int categoryId) {
        setData(DataContext.get(categoryId).getContentItem());

        this.context = context;
        this.categoryId = categoryId;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View vHolder = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_content_item, parent, false);
        return new ViewHolder(vHolder);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
       ContentItemModel currentElement = getItem(position);

        // Setup of the current element.
        holder.getItemTitle().setText(currentElement.getName());
        holder.getItemContent().setText(currentElement.getDescription());
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.item_title)
        TextView itemTitle;
        @BindView(R.id.item_content)
        TextView itemContent;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public TextView getItemTitle() {
            return itemTitle;
        }

        public TextView getItemContent() {
            return itemContent;
        }
    }
}
