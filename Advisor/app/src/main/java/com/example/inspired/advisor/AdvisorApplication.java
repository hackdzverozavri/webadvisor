package com.example.inspired.advisor;

import android.app.Application;
import android.content.Context;

import java.util.TimeZone;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class AdvisorApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        initLibs();

        // setting time zone for the database dates issue
        TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
    }

    public void initLibs() {
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/Roboto-Medium.otf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(base));
    }
}

