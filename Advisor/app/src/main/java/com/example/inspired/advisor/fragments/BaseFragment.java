package com.example.inspired.advisor.fragments;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import com.example.inspired.advisor.activities.BaseActivity;

/**
 * Created by Ivelin on 15.10.2016 г..
 */

public class BaseFragment extends Fragment {

    private BaseActivity mBaseActivity;
    private Context context;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context != null) {
            this.mBaseActivity = (BaseActivity) context;
            this.context = context;
        }
    }

    public BaseActivity getBaseActivity() {
        return this.mBaseActivity;
    }

    public Context getContext() {
        return this.context;
    }

    /**
     * Method for pop the stack
     */
    public void popBackFragment() {
        getActivity().getSupportFragmentManager().popBackStack();
    }

    /**
     * Method for replacing fragment and add to backstack
     *
     * @param fragment
     * @param name
     * @param resContainer
     */
    public void showFragmentAndAddToBackStack(Fragment fragment, String name, int resContainer) {
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        transaction.replace(resContainer, fragment);
        transaction.addToBackStack(name);
        transaction.commit();
    }

    /**
     * Method for adding fragment and add to BackStack
     *
     * @param fragment
     * @param name
     * @param resContainer
     */
    public void addFragmentAndAddToBackStack(Fragment fragment, String name, int resContainer) {
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        transaction.add(resContainer, fragment);
        transaction.addToBackStack(name);
        transaction.commit();
    }

    /**
     * Method for showing the fragment without clearing the BackStack or adding to BackStack
     *
     * @param fragment
     */
    public void showFragment(Fragment fragment, int resContainer) {
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        transaction.replace(resContainer, fragment);
        transaction.commit();
    }

    /**
     * Method for clearing the backstack
     *
     * @param fragment
     */
    public void clearBackstackAndReplaceFragment(Fragment fragment, int resContainer){
        getActivity().getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        transaction.replace(resContainer, fragment);
        transaction.commit();
    }
}
