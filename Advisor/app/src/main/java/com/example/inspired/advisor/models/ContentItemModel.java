package com.example.inspired.advisor.models;

/**
 * Created by inspired on 19.10.16.
 */

public class ContentItemModel {
    private String name;
    private String description;

    public ContentItemModel(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }
}
