package com.example.inspired.advisor.activities;

import android.os.Bundle;

import com.example.inspired.advisor.R;
import com.example.inspired.advisor.fragments.AdvisorFormFragment;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends BaseActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        replaceFragment(new AdvisorFormFragment(), "WelcomeFragment", R.id.container);
    }
}
