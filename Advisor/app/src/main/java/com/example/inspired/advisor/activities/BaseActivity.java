package com.example.inspired.advisor.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.example.inspired.advisor.fragments.BaseFragment;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class BaseActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    public void changeActivity(Context context, Class newActivity) {
        if (context != null && newActivity != null) {
            Intent mainIntent = new Intent(context, newActivity);
            context.startActivity(mainIntent);
            ((Activity) context).finish();
        }
    }

    public BaseFragment getCurrentDisplayedFragment(int resContainer) {
        return (BaseFragment) getSupportFragmentManager().findFragmentById(resContainer);
    }

    /**
     * Method for replacing fragment in container
     *
     * @param fragment
     * @param name
     */
    public void replaceFragment(Fragment fragment, String name, int resContainer) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(resContainer, fragment);
        transaction.commit();
    }

    /**
     * Method for showing back button if needed
     *
     * @param context
     * @param shouldShow
     */
    public void showBackButton(Context context, boolean shouldShow) {
        if (getSupportActionBar() != null) {
            getSupportActionBar().setHomeButtonEnabled(shouldShow);
            getSupportActionBar().setDisplayHomeAsUpEnabled(shouldShow);
        }
    }

    /**
     * Respond to the action bar's Up/Home button
     *
     * @param item
     * @return
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Change status bar color. We can have fragments with different status bar color
     *
     * @param color
     */
    public void changeStatusBarColor(int color) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(color);
        }
    }

    public void setSoftinputMode(int mode) {
        getWindow().setSoftInputMode(mode);
    }

    public void showToolbarShadowOnPreLolipop(View view) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            view.setVisibility(View.VISIBLE);
        }
    }

    public void initToolbar(Toolbar toolbar, View shadow){
        //changeStatusBarColor(ContextCompat.getColor(this, R.color.main_status_bar_color));
        //toolbar.setNavigationIcon(R.drawable.ic_back_button);
        showToolbarShadowOnPreLolipop(shadow);

        setSupportActionBar(toolbar);
    }
}