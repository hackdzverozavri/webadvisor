package com.example.inspired.advisor.adapters;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.inspired.advisor.R;
import com.example.inspired.advisor.activities.MainActivity;
import com.example.inspired.advisor.fragments.ResultsContentFragment;
import com.example.inspired.advisor.models.ChartItemModel;
import com.example.inspired.advisor.utils.StaticDataUtils;
import com.github.mikephil.charting.charts.LineChart;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by inspired on 18.10.16.
 */

public class ChartsAdapter extends BaseRecyclerViewAdapter<ChartsAdapter.ViewHolder, ChartItemModel> {
    private final Context context;
    private final ItemListener itemListener;

    public ChartsAdapter(Context context) {
        setData(StaticDataUtils.setStaticCharts());

        this.context = context;
        this.itemListener = new ItemListener() {
            @Override
            public void onItemClick(int position) {
                // Calling specific element fragment.
                startElementFragment(getItem(position).getId());
            }
        };
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View vHolder = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_chart_item, parent, false);
        return new ViewHolder(vHolder);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        ChartItemModel currentElement = getItem(position);

        // Setup of the current element.
        holder.getCategoryName().setText(currentElement.getCategoryName());
        holder.setItemListener(itemListener);
        StaticDataUtils.setChart(holder.getChart(), currentElement.getData());
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.category_name)
        TextView categoryName;
        @BindView(R.id.chart)
        LineChart chart;

        private ItemListener itemListener;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public TextView getCategoryName() {
            return categoryName;
        }

        public LineChart getChart() {
            return chart;
        }

        public void setItemListener(ItemListener itemListener) {
            this.itemListener = itemListener;
        }

        @OnClick(R.id.chart_element)
        public void onClick() {
            if (itemListener != null) {
                itemListener.onItemClick(getAdapterPosition());
            }
        }
    }

    private void startElementFragment(int itemId) {
        Fragment fragment = ResultsContentFragment.newInstance(itemId);

        FragmentTransaction transaction = ((MainActivity) context).getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.container, fragment);
        transaction.addToBackStack("Item");
        transaction.commit();
    }
}
