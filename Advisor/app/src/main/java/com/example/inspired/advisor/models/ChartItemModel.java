package com.example.inspired.advisor.models;

import com.example.inspired.advisor.R;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineDataSet;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by inspired on 19.10.16.
 */

public class ChartItemModel {
    private int id;
    private String categoryName;
    private LineDataSet data;


    public ChartItemModel(int id, String name, ArrayList<Integer[]> data) {
        this.categoryName = name;
        this.data = getChartData(data);
        this.id = id;
    }

    private LineDataSet getChartData(ArrayList<Integer[]> dataObjects){
        List<Entry> entries = new ArrayList<>();

        for (Integer[] data : dataObjects) {
            // turn your data into Entry objects
            entries.add(new Entry(data[0], data[1]));
        }

        LineDataSet dataSet = new LineDataSet(entries, "Label"); // add entries to dataset
        dataSet.setColor(R.color.colorAccent);
        dataSet.setValueTextColor(R.color.colorPrimaryDark);

        return dataSet;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public LineDataSet getData() {
        return data;
    }

    public int getId() {
        return id;
    }
}
