import numpy as np
import sys
import json
import os
from os import listdir
from os.path import isfile, join

def load_embeddings(glove_dir):
    embeddings_index = {}
    f = open(os.path.join(GLOVE_DIR, 'glove.6B.100d.txt'), encoding="utf8")
    for line in f:
        values = line.split()
        word = values[0]
        coefs = np.asarray(values[1:], dtype='float32')
        embeddings_index[word] = coefs
    f.close()
    return embeddings_index  

def load_drug_names(drug_names_file):
    drug_names = set()
    with open(drug_names_file) as f:
        content = f.readlines()
    for line in content:
        name = line.split(',')[5].strip('"')
        drug_names.add(name)
    return drug_names

def load_drugs_data(files):
    '''
    drug jivot
    '''
    drugs_data = []
    for file in files:
        drugs_data.append(load_file(file))
    return drugs_data

def load_file(file):
    with open(file) as data_file:    
        data = json.load(data_file)
    return data

def get_filenames(data_path):
    filenames = []
    for file in listdir(data_path):
      if (isfile(join(data_path, file))):
        filenames.append(join(data_path, file))     
    return filenames

BASE_DATASET_DIR = 'datasets'
GLOVE_DIR = BASE_DATASET_DIR + '/glove.6B/'
DRUG_NAMES_DIR = BASE_DATASET_DIR + '/drugs_data/Products.txt'
TEXT_DATA_DIR = BASE_DATASET_DIR + '/drugs_articles'

EMBEDDING_DIM = 100
MIN_DISTANCE = 5

# load embeddings
print('Load embeddings')
embeddings_index = load_embeddings(GLOVE_DIR)


# lopad drug names
print('Load drug names')
drug_names = load_drug_names(DRUG_NAMES_DIR)

# load scrapped data
print('Load scrapped data')
data_files = get_filenames(TEXT_DATA_DIR)
drugs_data = load_drugs_data(data_files)
