import numpy as np
from nltk import word_tokenize

from data import embeddings_index
from data import drugs_data
from data import drug_names

def euclidean_distance(a, b):
    dist = (a-b)**2
    dist = dist.sum(axis=-1)
    dist = np.sqrt(dist)
    return dist

def rating_function(old_rating, word, words_num, category, embeddings_index):
    if word in embeddings_index:
        v1 = embeddings_index[word]
        v2 = embeddings_index[category]
        distance = euclidean_distance(v1, v2)
        if (distance < MAX_DISTANCE and distance != 0.0):
            return old_rating + 1.0 / (1 + distance + words_num)
    return old_rating

MAX_DISTANCE = 5
LABELS = ['headache', 'depression', 'infection', 'pain', 'death', 'insomnia', 'tiredness']

drug_names = list(drug_names)

i = 0
cat_to_drug = {}
ans_to_drug_and_cat = {}
for x in drugs_data[0]:
    title = x['title']
    articles = x['articles']
    # print(title)
    for category in LABELS:
        # print(category)
        drug_and_cat_to_answ = {}
        for drug in drug_names:
            if len(drug) > 3:
                cat_to_drug[(category, drug)] = cat_to_drug.get((category, drug), 0.0) # for all
                for answer in articles:
                    if (drug in title or drug in answer):
                        # print('article: %d' % i)
                        words = word_tokenize(answer)
                        words_num = len(words)
                        for word in words:
                            cat_to_drug
                            cat_to_drug[(category, drug)] = rating_function(cat_to_drug[(category, drug)],
                                                                            word,
                                                                            words_num,
                                                                            category,
                                                                            embeddings_index)
                        if cat_to_drug[ans_to_drug_and_cat.get(answer, (category, drug))] <= cat_to_drug[(category, drug)]:
                            ans_to_drug_and_cat[answer] = (category, drug)

print("load into db")
from neo4j.v1 import GraphDatabase,basic_auth
import re

driver = GraphDatabase.driver("bolt://localhost:7687", auth=basic_auth("neo4j", "neo4j"))
session = driver.session()

# print(ans_to_drug_and_cat)

c = set()
d = set()
for (category, drug), rating in cat_to_drug.items():
    if rating > 0:
        if not drug in d:
            session.run("CREATE (a:drug { name : '%s' })" % drug)
        if not category in c:
            session.run("CREATE (a:category { name : '%s' })" % category)
        c.add(category)
        d.add(drug)
        session.run("MATCH (drug),(category) WHERE drug.name = '%s' AND category.name = '%s' CREATE (drug)-[ :CAUSES { rating : %d } ]->(category)" % (drug, category, rating))

for answer, (cat, drug) in ans_to_drug_and_cat.items():
    # print("CREATE (drug { name : '%s' })" % drug_name)\
    session.run("CREATE (a:answer { text : '%s', source : '' })" % re.sub("'", '', answer))
    session.run("MATCH (answer),(drug) WHERE answer.text = '%s' AND drug.name = '%s'CREATE (answer)-[ :FOR_DRUG ]->(drug)" % (re.sub("'", '', answer), drug))
    session.run("MATCH (answer),(category) WHERE answer.text = '%s' AND category.name = '%s' CREATE (answer)-[ :FOR_CATEGORY ]->(category)" % (re.sub("'", '', answer), cat))
session.close()