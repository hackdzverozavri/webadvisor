import scrapy
import re

class QuotesSpider(scrapy.Spider):
    urlBegin = "https://www.drugs.com/forum/featured-drugs/index"
    urlEnd = ".html"
    all_pages = []
    for i in range(23, 40):
        all_pages.append(urlBegin + str(i) + urlEnd)

    name = "quotes"
    start_urls = all_pages

    def parse(self, response):
        all_topics = response.css('a.title::attr(href)').extract()
        for topic in all_topics:
            yield scrapy.Request(topic, callback=self.parse1)

    def parse1(self, response):
        allArticles = []
        for quote in response.css("blockquote.postcontent.restore").extract():
            p = re.compile(r'<.*?>')
            tmp = p.sub('', quote)
            allArticles.append(tmp)
        title = response.css("title::text").extract_first()
        
        next_page_url = response.css('span.prev_next a[rel=next]::attr(href)').extract_first()
        if next_page_url is not None:
            yield scrapy.Request(next_page_url, callback=self.parse1)
        yield {'title': title, 'articles': allArticles}