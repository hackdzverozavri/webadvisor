import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent }  from './app.component';
import { SearchComponent }  from './searchbar.component';
import { ResultAreaComponent }  from './resultarea.component';
import { GraphicsComponent }  from './graphics.component';
import { CategoriesComponent } from './categories.component';
import { DrugDescriptionComponent } from './drugdescription.component';
import { FormsModule }   from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import {HttpModule} from '@angular/http';

const appRoutes: Routes = [
  { path: 'app/result/:id', component: DrugDescriptionComponent },
  { path: 'app/result', component: ResultAreaComponent }
];

@NgModule({
  imports:      [ BrowserModule, FormsModule, RouterModule.forRoot(appRoutes), HttpModule],
  declarations: [ AppComponent,
                  ResultAreaComponent, 
                  SearchComponent, 
                  GraphicsComponent, 
                  CategoriesComponent,
                  DrugDescriptionComponent 
                ],
  bootstrap:    [ AppComponent ]
})
export class AppModule { }
