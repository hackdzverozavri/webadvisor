import { Component } from '@angular/core';
import { CategoriesComponent } from './categories.component';
import { ResultAreaComponent } from './resultarea.component';
import { GraphicsComponent }  from './graphics.component';
import { DrugDescriptionComponent } from './drugdescription.component';
@Component({
  selector: 'my-app',
  template: `
    <div class="row">
      <search-box></search-box>
    </div>
  `,
})
export class AppComponent  {  }
