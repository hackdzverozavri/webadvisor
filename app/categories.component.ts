import {Component} from '@angular/core';

@Component({
    moduleId: module.id,
    selector:'categories',
    templateUrl: 'categories.html',
    styles:[`
        .center{
            margin-left:200px;
        }
    `]
})
export class CategoriesComponent {

    constructor(){

    }

    private log: string ='';

    private logCheckbox(element: HTMLInputElement): void {
        this.log += `Checkbox ${element.value} was ${element.checked ? '' : 'un'}checked\n`
    }
}