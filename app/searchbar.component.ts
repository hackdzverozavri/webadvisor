import { Component, Input } from '@angular/core';

import { FormsModule }   from '@angular/forms';

@Component({
    moduleId: module.id,
    selector: 'search-box',
    templateUrl: 'searchbar.html',
    styleUrls: ['searchbar.css']
})

export class SearchComponent {

    constructor(){
       
    }

     private drugName : string = '';
     private log: string ='';
     private checkboxData: Array<HTMLInputElement> = [];
     private  data: Array<string> = [];

    private getData(element: HTMLInputElement): void {
        if(element.type === 'checkbox'){
        this.checkboxData.push(element);
        console.log(this.data)
        this.data.push(element.name);
        this.log += this.checkboxData.join(',')

        }else{
            this.drugName = element.value;
            element.checked = false;
            this.data=[];

           this.checkboxData.forEach(cb => {
               cb.checked = false;
           })
        }
    }
}