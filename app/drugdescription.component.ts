import { Component, Input } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
 
@Component({
    moduleId: module.id,
    selector: 'drug-description',
    templateUrl: 'drugdescription.html',
    styles:[`
        p{
            padding-right: 50px;
        }
    `]
})
export class DrugDescriptionComponent{
   private pid : any;
    constructor( route: ActivatedRoute){
            this.pid = route.snapshot.params['id'];
            console.log(route)
    }

}