import {Component, Input, Output, EventEmitter} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {Http, Response} from '@angular/http';

@Component({
    selector: 'graphics',
    template: `<div 
               routerLink="/app/result/{{number}}"
                routerLinkActive="active"
                id='graphics{{number}}'
                >
                
             </div>
    `,
    styles: [`
       
        div{
            padding-top: 40px;
            margin-left: 100px;
        }
    `]
})

export class GraphicsComponent {

    @Input() number : number;
     obj : any;
     @Output() a : any = "";
    // @Output() onsendData = new EventEmitter<any>();

    ngOnInit(){

         this.http.get('app/assets/fakedata'+ (this.number)+'.json').forEach(res =>{
            this.obj = res.json();
        }).then(data => {
            this.loadGraphics(this.number, this.obj)
            this.a = this.obj["meta"]["category"]
            console.log(this.number)

        });
    }
  
    constructor(private http : Http){
               
    }

    loadGraphics(id:number, data:JSON){
            MG.data_graphic({
                title: data["meta"]["category"],
                description: "A graphic where we're not plotting dates on the x-axis and where the axes include labels and the line animates on load. We've also enabled extended ticks along the y-axis.",
                data: data["data"],
                animate_on_load: true,
                area: true,
                width: 500,
                height: 200,
                right: 40,
                left: 90,
                bottom: 50,
                y_extended_ticks: true,
                target: '#graphics'+id,
                x_accessor: 'drug',
                y_accessor: 'category',
                x_label: 'category reiting',
                y_label: 'drug reiting',
            });
    }

    
}